package com.cuartob.adivinasemana;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import java.util.Calendar;

public class semana extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_semana);

        TextView lblresultado = (TextView) findViewById(R.id.lblresultado);
        //resivir el bundle
        Bundle bundle= this.getIntent().getExtras();
        //guardar en variable string el parametro recibido de la interfaz anterior tiene que ser la misma clave ingresada en la anterior
        String semanausuario= bundle.getString("semana");
        //Conversión de String a Int (entero a cadena)
        int semanausuarioint = Integer.parseInt(semanausuario);
        //Clase calendario
        Calendar calendario= Calendar.getInstance();
        //Variable de tipo entera donde se guarda la semana del año, que da el metodo de android
        int semanareal= calendario.get(Calendar.WEEK_OF_YEAR);

        /*

        //Para mostrar en el logcat la semana que es

        String semanaReal = Integer.toString(semanareal);
        Log.e("semana", semanaReal);
        */


        //Comparación para saber si el usuario acertó
        if (semanausuarioint==semanareal){
            //Mostrar el mensaje en el textView
            lblresultado.setText("SEMANA ACTUAL");
            //Metodo para cambiar el color

        }
        else{
            lblresultado.setText("SEMANA INCORRECTA");

        }

    }
}
