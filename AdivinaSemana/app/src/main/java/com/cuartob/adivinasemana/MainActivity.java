package com.cuartob.adivinasemana;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {
    EditText txtsemana;
    Button btnpasar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        txtsemana= (EditText) findViewById(R.id.txtsemana);
        btnpasar= (Button) findViewById(R.id.btnpasar);
        btnpasar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //metodo para pasar a otra interfaz
                Intent intent = new Intent(MainActivity.this,semana.class);
                //metodo para pasar parametro
                Bundle bundle= new Bundle();
                //pasar el parametro identificado por una clave entre comillas
                bundle.putString("semana",txtsemana.getText().toString());
                //pasar el parametro mediante el intent
                intent.putExtras(bundle);
                //iniciar la otra interfaz
                startActivity(intent);
            }
        });
    }
}
